function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

function WelcomeApp() {
  return (
    <div>
      <Welcome name="Sara" />
      <Welcome name="Cahal" />
      <Welcome name="Edite" />
    </div>
  );
}

/**
 * Function & Class
 */
function Clock(props) {
  return (
    <div>
      <h1>Hello, world!</h1>
      <h2>It is {props.date.toLocaleTimeString()}.</h2>
    </div>
  );
}

class MyClock extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {props.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

const App = () => {
  const [count, setCount] = React.useState(0);

  return (
    <div>
      <div>How many counts? {count}</div>
      <button onClick={() => setCount(c => c+1)}>Increment +</button>
      <button onClick={() => setCount(c => c+1)}>Decrement -</button>
    </div>
  );
};


ReactDOM.render(
  <App />,
  document.getElementById('root')
);

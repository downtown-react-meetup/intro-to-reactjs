# Intro to ReactJS

A basic introduction into wiring & writing ReactJS views for beginners.

## Setup

### Disable CORS:

#### Firefox

`https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp`

#### Chrome

`https://alfilatov.com/posts/run-chrome-without-cors/`

### Development

* Install **React Developer Tools** https://reactjs.org/blog/2019/08/15/new-react-devtools.html
* Add the following script tags to the `<head>`

```html
<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
<script crossorigin src="https://unpkg.com/@babel/standalone/babel.min.js"></script>
```

**note**: *Babel compiles JSX down to `React.createElement()` calls.*

## Getting Started

### JSX

curly braces for interpolation - embed any expression

attributes - camelCase, `className` — are how an element/component receives data

### Elements

Components are made of elements

Immutable

### Components & Props

> Components let you split the UI into independent, reusable pieces, and think about each piece in isolation.

Components as JS functions

```javascript
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```

* f(STATE) = VIEW
* f(DATA) = VIEW
* f(PROPS) = VIEW

Components as JS classes

```javascript
class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```

Either way, React returns `elements` that describe what should appear on the screen.

**note:** Component names start with a capital letter

> We recommend naming props from the component’s own point of view rather than the context in which it is being used.

**note:** Data model & Component model do not need to match but they do need to be compatible

> A good rule of thumb is that if a part of your UI is used several times (`Button`, `Panel`, `Avatar`), or is complex enough on its own (`App`, `FeedStory`, `Comment`), it is a good candidate to be a reusable component.

Props **must** never be mutated

> **All React components must act like pure functions with respect to their props.**

### State & Lifecycle

> State is similar to props, but it is private and fully controlled by the component.

> Neither parent nor child components can know if a certain component  is stateful or stateless, and they shouldn’t care whether it is defined  as a function or a class.

> This is why state is often called local or encapsulated. It is not  accessible to any component other than the one that owns and sets it.

Data flows down - Unidirectional

### Handing Events

* React events follow camelCase naming
* With JSX you pass a function as the event handler rather than a string

[Synthetic Events](https://reactjs.org/docs/events.html)

### Conditional Rendering

`if-else`

`true && expression`

`condition ? true : false`

returning `null` does not render the component

> Returning `null` from a component’s `render` method does not affect the firing of the component’s lifecycle methods. For instance `componentDidUpdate` will still be called.

### List & Keys

assign `key` ; must be unique among siblings

> Keys help React identify which items have changed, are added, or are removed.

### Forms

**Controlled Components**

* combine element state with component state
* every state mutation will have an associated handler function

### Lifting State Up

> There should be a single “source of truth” for any data that changes in a React application. Usually, the state is first added to the component  that needs it for rendering. Then, if other components also need it, you can lift it up to their closest common ancestor. Instead of trying to  sync the state between different components, you should rely on the [top-down data flow](https://reactjs.org/docs/state-and-lifecycle.html#the-data-flows-down).


#### Good-to-knows

* JS implementation & concepts
  * object destructuring
  * arrow functions
  * shallow merge (Object.assign, Object spread)

* write code for humans / write readable code
* we all come to the web from different learning paths:
* our words will not always mean the same things
* our mental models will have different shapes
* communicate to learn / communicate to share / communicate to help
* be patient / be nice / be humble
 
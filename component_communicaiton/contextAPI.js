const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

const ThemeContext = React.createContext(themes.light);

function App() {
  return (
    <ThemeContext.Provider value={themes.light}>
      <Toolbar />
    </ThemeContext.Provider>
  );
}

function Toolbar(props) {
  return (
    <div>
      <ThemedButton />
    </div>
  );
}

function ThemedButton() {
  const theme = React.useContext(ThemeContext);

  return (
    <React.Fragment>
    <button style={{ background: theme.background, color: theme.foreground }}>
      I am styled by theme context!
    </button>
    <br />
    <button style={{ background: theme.background, color: theme.foreground }}>
      change theme
    </button>
    </React.Fragment>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

// add a function that changes the theme when the `change theme` button is pressed

const book = {
  title: 'Art Direction for the Web',
  author: {
    name: 'Andy Clarke',
    residence: 'Flintshire, North Wales, UK',
    image: 'https://res.cloudinary.com/malarkey/image/upload/v1515449087/img-portrait_taleq5.jpg',
  },
  publisher: {
    name: 'Smashing Magazine',
    url: 'https://www.smashingmagazine.com/'
  },
  publicationDate: 2019
};

function AuthorInfo({ authorName, residence, image, setName }) {
  const handleNameChange = e => {
    setName(e.target.value);
  };

  return (
    <div>
    {/* <form> */}
      <label>
        Name:
        <input type="text" value={authorName} onChange={handleNameChange} />
      </label>
      <h3>{residence}</h3>
      <img src={image} alt="author image" />
      {/* <button type="submit" onSubmit>Save Changes</button> */}
    {/* </form> */}
    </div>
  );
}

function PublisherInfo({ publisher }) {
  const { name, url } = publisher;
  return (
    <div>
      <h1>{name}</h1>
      <h2>{url}</h2>
    </div>
  );
}

function BookInfo({ book, authorName, setName }) {
  const { title, author, publisher, publicationDate } = book;
  const { residence, image } = author;

  return (
    <div>Book Info
      <h1>{title}</h1>
      <AuthorInfo authorName={authorName} residence={residence} image={image} setName={setName} />
      <PublisherInfo publisher={publisher} />
    </div>
  );
}

function PropDrilling({ book }) {
  const { title, author, publisher, publicationDate } = book;
  const [name, setName] = React.useState(author.name);

  return (
    <div>
      {`authors name: ${name}`}
      <BookInfo book={book} authorName={name} setName={setName} />
    </div>
  );
}

ReactDOM.render(
  <PropDrilling book={book} />,
  document.getElementById('root')
);

function ParentOne({ handleMessagePassing, data }) {
  const { from, message } = data;
  const [msg, setMsg] = React.useState('say something to parent two');

  return (
    <div>
      <h3>Hi. I'm Parent One</h3>
      {from === 'two' && <div>message from parent two: {message}</div>}
      <Envelope from="one" handleMessagePassing={handleMessagePassing} />
    </div>
  );
}

function Envelope({ from, handleMessagePassing }) {
  const [msgValue, setMsgValue] = React.useState('');

  const handleSubmit = e => {
    e.preventDefault();
    handleMessagePassing({ from, message: msgValue });
  };

  const handleMsgChange = e => {
    setMsgValue(e.target.value);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <label>
          say something to parent {from}
          <input type="text" value={msgValue} onChange={handleMsgChange} />
          <input type="submit" value="Submit" />
        </label>
      </form>
    </>
  );
}

function ParentTwo({ handleMessagePassing, data }) {
  const { from, message } = data;
  const [msg, setMsg] = React.useState('say something to parent one');

  return (
    <div>
      <h3>Hi. I'm Parent Two</h3>
      {from === 'one' && <div>message from parent one: {message}</div>}
      <Envelope from="two" handleMessagePassing={handleMessagePassing} />
    </div>
  );
}

function App() {
  const [data, setData] = React.useState({
    from: 'one',
    message: 'say something'
  });

  const handleMessagePassing = _data => {
    const { from, message } = _data;
    setData({ from, message });
  };


  return (
    <div>
      <ParentOne handleMessagePassing={handleMessagePassing} data={data} />
      <ParentTwo handleMessagePassing={handleMessagePassing} data={data} />
    </div>
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

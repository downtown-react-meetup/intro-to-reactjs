function PresentationalComponentOne() {
  return (
    <div className="pres-comp-one">
      <span>Presentational Component One</span>
      <p>I do nothing but render!</p>
    </div>
  );
}

function PresentationalComponentTwo() {
  const [data, setData] = React.useState('hold this for me');

  return (
    <div className="pres-comp-two">
      <span>Presentational Component Two</span>
      <p>I create my own state & pass it down</p>
      <PresentationalComponentThree data={data} />
      <p>more data down here!!</p>
    </div>
  );
}

function PresentationalComponentThree({ data }) {
  return (
      <div className="pres-comp-three">
      <span>Presentational Component Three</span>
      <p>I receive data from above & render it</p>
      <div>
        {data}
      </div>
    </div>
  );
}

function App() {
  return (
    <div>
      <PresentationalComponentOne />
      <PresentationalComponentTwo />
    </div>
  );
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);


function SidePanelOne() {
  return (
    <div className="side-panel-one">
      <h1>Side Panel One</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis vulputate nisi vitae consequat. Nullam accumsan arcu nec lacus tincidunt ultrices. Mauris id ornare nisi. Sed molestie consequat risus ut faucibus. Proin at ornare lectus, sit amet pharetra est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam ultrices placerat urna et rhoncus.</p>
    </div>
  );
}

function SidePanelTwo() {
  return (
    <div className="side-panel-two">
      <h1>Side Panel Two</h1>
      <p> Duis tristique lobortis porta. Morbi sit amet lectus nec purus sodales laoreet ac ac risus. Phasellus eget finibus eros, nec cursus libero. Maecenas placerat faucibus sapien id faucibus. Donec justo ex, blandit vitae dictum ut, mollis nec ligula. Pellentesque eros purus, finibus quis est ac, eleifend commodo orci. Nam id neque tempus, tristique dui sit amet, aliquam enim. Praesent turpis neque, molestie in nisi ut, varius feugiat nunc.</p>
    </div>
  );
}

function LayoutComponentTB({ children}) {
  return (
    <section className="layout-component-tb">
      {children}
    </section>
  );
}

function LayoutComponentLR({ children}) {
  return (
    <section className="layout-component-lr">
      {children}
    </section>
  );
}

function App() {
  return (
    <React.Fragment>
      <LayoutComponentLR>
        <SidePanelOne />
        <SidePanelTwo />
      </LayoutComponentLR>
    </React.Fragment>
  );
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
);

function UserGreeting({}) {
  return <h1>Welcome back!</h1>;
}

function GuestGreeting({}) {
  return <h1>Please sign up.</h1>;
}

function Greeting({isLoggedIn}) {
  if (isLoggedIn) {
    return <UserGreeting />;
  }

  return <GuestGreeting />;
}

function LoginButton(props) {
  return (
    <button onClick={props.onClick}>
      Login
    </button>
  );
}

function LogoutButton(props) {
  return (
    <button onClick={props.onClick}>
      Logout
    </button>
  );
}

function ContainerComponent() {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);

  let button;

  if (isLoggedIn) {
    button = <LogoutButton onClick={() => setIsLoggedIn(false)} />;
  } else {
    button = <LoginButton onClick={() => setIsLoggedIn(true)} />;
  }

  return (
    <div>
      <Greeting isLoggedIn={isLoggedIn} />
      {button}
    </div>
  );
}


ReactDOM.render(
  <ContainerComponent />,
  document.getElementById('root')
);

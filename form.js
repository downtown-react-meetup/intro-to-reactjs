function NameForm() {
  const [nameValue, setNameValue] = React.useState('');
  const [selectValue, setSelectValue] = React.useState('');
  const [isGoing, setIsGoing] = React.useState(true);
  const [numberOfGuests, setNumberOfGuests] = React.useState(2);

  const handleSubmit = e => {
    e.preventDefault();
    alert(`A name was submitted: ${JSON.stringify({ nameValue, selectValue, isGoing, numberOfGuests })}`);
  };

  const handleNameChange = e => {
    setNameValue(e.target.value);
  };
  const handleSelectChange = e => {
    setSelectValue(e.target.value);
  };
  const handleGuestChanges = e => {
    const { type, name, checked, value } = e.target;
    type === 'checkbox'
      ? setIsGoing(checked)
      : setNumberOfGuests(value)
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={nameValue} onChange={handleNameChange} />
      </label>

      <label>
        Pick your favorite flavor:
        <select value={selectValue} onChange={handleSelectChange}>
          <option value="grapefruit">Grapefruit</option>
          <option value="lime">Lime</option>
          <option value="coconut">Coconut</option>
          <option value="mango">Mango</option>
        </select>
      </label>

      <label>
          Is going:
          <input
            name="isGoing"
            type="checkbox"
            checked={isGoing}
            onChange={handleGuestChanges} />
        </label>
        <br />
        <label>
          Number of guests:
          <input
            name="numberOfGuests"
            type="number"
            value={numberOfGuests}
            onChange={handleGuestChanges} />
        </label>
      <input type="submit" value="Submit" />
    </form>
  );
}


ReactDOM.render(
  <NameForm />,
  document.getElementById('root')
);
